package fr.soleil.util.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.soleil.util.tree.visitor.IVisitor;

/**
 * 
 * This class represents a node element in a tree.
 * 
 * @author MOULHAUD
 */

public abstract class TreeNodeElement extends ATreeElement implements ITreeNodeElement {
    // This is the list of children for the node
    protected List<ATreeElement> children = new ArrayList<ATreeElement>();

    /**
     * Construct the TreeNodeElement with his name
     * 
     * @param value
     */
    public TreeNodeElement(Object value) {
        super(value);
    }

    /**
     * Add a child for a TreeNodeElement
     * 
     * @param element {@link ATreeElement}
     */

    @Override
    public void addChild(ATreeElement element) {
        children.add(element);
    }

    /**
     * Add a list of ATreeElement
     * 
     * @param elementList
     */
    public void addChildren(Collection<ATreeElement> elementList) {
        if (elementList != null) {
            children.addAll(elementList);
        }
    }

    /**
     * @param i : the child index
     * @return : {@link ATreeElement}
     */

    @Override
    public ATreeElement getChildAt(int i) {
        ATreeElement child;
        if ((children == null) || i < 0 || i >= children.size()) {
            child = null;
        } else {
            child = children.get(i);
        }
        return child;
    }

    /**
     * To get the children list for the current node
     * 
     * @return {@link ArrayList} : the children list
     */

    @Override
    public List<ATreeElement> getChildren() {
        return children;
    }

    /**
     * @return int : the number of childs
     */
    @Override
    public int getChildsCount() {
        return children == null ? 0 : children.size();
    }

    /**
     * Receive a visitor, and call the visit method of it.
     * 
     * @param visitor
     */
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);

    }

    /**
     * Remove the ATreeElement from the list of child
     */
    @Override
    public void remove(ATreeElement element) {
        if ((children != null) && (element != null)) {
            children.remove(element);
        }
    }

    /**
     * Remove all element from the Tree Node Element
     *
     */
    public void removeAll() {
        children = new ArrayList<ATreeElement>();
    }
}
