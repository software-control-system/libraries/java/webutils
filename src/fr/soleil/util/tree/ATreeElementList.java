package fr.soleil.util.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This list contains a group of ATreeElement.
 * It can be used to store different tree and manage them
 * 
 * @author BARBA-ROSSA
 *
 */
public class ATreeElementList {
    private final List<ATreeElement> m_list = new ArrayList<ATreeElement>(); // the list
    private final Map<String, ATreeElement> m_map = new HashMap<String, ATreeElement>(); // The map

    /**
     * Add a Tree in the end of the list
     * 
     * @param newATreeElement
     */
    public void addATreeElement(ATreeElement newATreeElement) {
        m_list.add(newATreeElement);
    }

    /**
     * Get a tree by it's index
     * 
     * @param index
     * @return ATreeElement
     */
    public ATreeElement getATreeElementbyIndex(int index) {
        return m_list.get(index);
    }

    /**
     * Add a key for a Tree.
     * The key may be use to retreive the Tree by it
     * 
     * @param strKey
     * @param elementIndex
     */
    public void addATreeElementKey(String strKey, int elementIndex) {
        ATreeElement newATreeElement = getATreeElementbyIndex(elementIndex);
        m_map.put(strKey, newATreeElement);
    }

    /**
     * Get a Tree by it's key.
     * The method <code>ATreeElementList.addATreeElementKey</code> must be call before.
     * 
     * @param strKey
     * @return {@link ATreeElement}
     */
    public ATreeElement getATreeElementbyKey(String strKey) {
        return m_map.get(strKey);
    }

    /**
     * Return the size of the list
     * 
     * @return int
     */
    public int size() {
        return m_list == null ? 0 : m_list.size();
    }

    /**
     * Remove all elements from this list
     *
     */
    public void clear() {
        if (m_list != null) {
            m_list.clear();
        }
        if (m_map != null) {
            m_map.clear();
        }
    }

}
