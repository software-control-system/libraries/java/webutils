package fr.soleil.util.internationalisation;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import fr.soleil.util.UtilLogger;

/**
 * This class Manage the Label for a list of locale
 * 
 * @author BARBA-ROSSA
 *
 */
public class LabelManager {
    // Name of the properties file
    public final static String s_strLABEL_FILE = "MessageBundle";

    private static HashMap<Locale, Properties> m_labels = new HashMap<Locale, Properties>();
    private static HashMap<Locale, Locale> m_errors = new HashMap<Locale, Locale>();

    /**
     * Reinit LabelManager contend
     *
     */
    public static void reinit() {
        m_labels = new HashMap<Locale, Properties>();
        m_errors = new HashMap<Locale, Locale>();
    }

    /**
     * Return true if the locale exist
     */
    public static boolean exist(Locale locale) {
        boolean exists = false;
        // first, we check if the locale is not in the errors map
        if (!m_errors.containsKey(locale)) {
            // we return if the locale exist
            exists = m_labels.containsKey(locale);
        }
        return exists;
    }

    /**
     * Return the labels for the selected locale and selected key
     * 
     * @param locale
     * @param strKey
     * @return String
     */
    public static String getLabels(Locale locale, String strKey) {
        String label;
        if (m_errors.containsKey(locale)) {
            // we check if the locale is not in the errors map, if the locale is in the error map, we will throw an
            // exception
            label = null;
        } else {
            if (!m_labels.containsKey(locale)) {
                // We check if the locale is already loaded
                readFile(locale);
            }

            // We get the labels
            Properties properties = m_labels.get(locale);
            if (properties == null) {
                label = null;
            } else {
                label = (String) properties.get(strKey);
            }
        }
        // if the label is not found we return a null value
        return label;
    }

    /**
     * Return the labels for the selected locale and selected key
     * 
     * @param locale
     * @param strKey
     * @param aDynValues : this array contains all dynbamique values to put into the labels. all $1, $2, $3, ... will be
     *            replace by the values in the array
     * @return String
     */
    public static String getLabels(Locale locale, String strKey, String[] aDynValues) {
        // we get the label
        String strLabel = getLabels(locale, strKey);
        // we replace the joker ($1, $2, $3, ... ) by the values
        if (aDynValues != null) {
            String exp = "\\$";
            for (int i = 0; i < aDynValues.length; i++) {
                strLabel = strLabel.replaceAll(exp + i, aDynValues[i]);
            }
        }
        return strLabel;
    }

    /**
     * This method load a LabelFile and add it in the labels map.
     * Return : true if loading with success the file, false if an exception occured
     * 
     * @param locale
     * @return boolean
     */
    private static boolean readFile(Locale locale) {
        InputStream stream = null;
        BufferedInputStream bufStream = null;
        boolean result;
        try {
            String strLocale = ""; // String use to specifie the locale when loading the file
            if (locale != null) {
                if (locale.getLanguage() != null)
                    strLocale = "_" + locale.getLanguage();
                if (locale.getCountry() != null)
                    strLocale += "_" + locale.getCountry();
            }

            // We use the class loader to load the properties file.
            // This compatible with unix and windows.
            stream = LabelManager.class.getClassLoader().getResourceAsStream(
                    s_strLABEL_FILE + strLocale + ".properties");
            Properties properties = new Properties();

            // We read the data in the properties file.
            if (stream != null) {
                // We need to use a Buffered Input Stream to load the datas
                bufStream = new BufferedInputStream(stream);
                properties.clear();
                properties.load(bufStream);
                m_labels.put(locale, properties);
            }
            result = true;
        } catch (IOException ioe) {
            m_errors.put(locale, locale);
            ioe.printStackTrace();
            result = false;
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            try {
                if (bufStream != null)
                    bufStream.close();
                if (stream != null)
                    stream.close();
            } catch (Exception e) {
                UtilLogger.logger.addERRORLog("LabelManager, error when closing properties file stream");
            }
        }
        return result;

    }
}
