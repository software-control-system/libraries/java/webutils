package fr.soleil.util.tabbedpane;

import java.awt.Color;
import java.awt.Container;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public abstract class TabbedPaneUICLoseButton extends BasicTabbedPaneUI {
    // override to return our layoutmanager
    @Override
    protected LayoutManager createLayoutManager() {
        return new CloseTabTbpLayout();
    }

    public abstract void closeTab(int iTab);

    // add 40 to the tab size to allow room for the close button and 8 to the height
    @Override
    protected Insets getTabInsets(int tabPlacement, int tabIndex) {
        // note that the insets that are returned to us are not copies.
        Insets defaultInsets = (Insets) super.getTabInsets(tabPlacement, tabIndex).clone();
        defaultInsets.right += 40;
        defaultInsets.top += 4;
        defaultInsets.bottom += 4;
        return defaultInsets;
    }

    protected class CloseTabTbpLayout extends TabbedPaneLayout {
        // a list of our close buttons
        List<CloseButton> closeButtons = new ArrayList<CloseButton>();

        @Override
        public void layoutContainer(Container parent) {
            super.layoutContainer(parent);

            // ensure that there are at least as many close buttons as tabs
            while (tabPane.getTabCount() > closeButtons.size()) {
                closeButtons.add(new CloseButton(closeButtons.size()));
            }

            Rectangle rect = new Rectangle();

            int i;
            for (i = 0; i < tabPane.getTabCount(); i++) {
                rect = getTabBounds(i, rect);
                JButton closeButton = closeButtons.get(i);
                // shift the close button 3 down from the top of the pane and 20 to the left
                closeButton.setLocation(rect.x + rect.width - 20, rect.y + 5);
                closeButton.setSize(15, 15);
                tabPane.add(closeButton);
            }

            for (; i < closeButtons.size(); i++) {
                // remove any extra close buttons
                tabPane.remove(closeButtons.get(i));
            }
        }

        // implement UIResource so that when we add this button to the
        // tabbedpane, it doesn't try to make a tab for it!

        protected class CloseButton extends JButton implements javax.swing.plaf.UIResource {

            private static final long serialVersionUID = -4358394437409211094L;

            public CloseButton(int index) {
                super(new CloseButtonAction(index));
                setToolTipText("Close this tab");
                this.setRolloverEnabled(true);
                this.setRolloverIcon(new ImageIcon(TabbedPaneUICLoseButton.class.getResource("icone_closeTab.gif")));
                // remove the typical padding for the button

                setMargin(new Insets(0, 0, 0, 0));
                addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        setForeground(new Color(255, 0, 0));
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        setForeground(new Color(0, 0, 0));
                    }
                });
            }
        }

        class CloseButtonAction extends AbstractAction {

            private static final long serialVersionUID = 5054959192463448284L;

            int index;

            public CloseButtonAction(int index) {
                super("", new ImageIcon(TabbedPaneUICLoseButton.class.getResource("img_closeTabDisable.gif")));
                this.index = index;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                closeTab(index);
            }
        } // End of CloseButtonAction
    } // End of CloseTabTbpLayout
} // End of static class TestPlaf
