package fr.soleil.util.serialized;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import fr.soleil.util.UtilLogger;
import fr.soleil.util.exception.SoleilException;
import fr.soleil.util.exception.WebSecurityException;

/**
 * Provide service to send a WebRequest to a WebServer and return the
 * WebResponse
 * 
 * @author BARBA-ROSSA
 * 
 */
public class WebServerClient implements IWebServerClient {
    private String m_strApplication = null;

    // We store here the server sessionid
    private String m_strJsessionid = null;

    // it's the WebServeur URL
    private URL m_Url = null;

    // Recall manager used to launch a new web request
    private IRecallManager m_recallManager = null;

    // LoginAction used to authenticate a user not already authenticated
    private ILoginAction m_iLoginAction = null;

    /**
     * Default constructor with the Web Server URL
     * 
     * @param newUrl
     */
    public WebServerClient(final URL newUrl) {
	m_Url = newUrl;
    }

    private static int i = 0;

    /**
     * Send a web Request and returns a response if ok
     * 
     * @param webRequest
     * @param con
     * @return {@link WebResponse}
     * @throws Exception
     *             if an exception occurs
     */
    private synchronized WebResponse sendWebRequest(final WebRequest webRequest) throws Exception {

	WebResponse response = null;
	OutputStream outstream = null;
	URLConnection con = null;
	try {
	    i++;
	    UtilLogger.logger.addInfoLog("WebServerClient.getObject begin :" + m_Url);
	    UtilLogger.logger.addInfoLog("WebServerClient.getObject action :" + webRequest.getAction());
	    // webRequest.getApplication());
	    final Object[] args = webRequest.getArguments();
	    // System.out.println(i + "########### sendWebRequest args " +
	    // args.length);
	    if (args.length >= 1) {
		// System.out.println(args[0].getClass());
		if (args[0] instanceof WebReflectRequest) {
		    // System.out.println(args[0]);
		    // System.out.println("action:" + ((WebReflectRequest)
		    // args[0]).getAction());
		    // System.out.println("method:" + ((WebReflectRequest)
		    // args[0]).getMethod());
		    // System.out.println("params:" +
		    // Arrays.toString(((WebReflectRequest)
		    // args[0]).getMethodParam()));
		}
	    }
	    // We open the connection to the server
	    con = getServletConnection("ActionServlet");
	    // We open � output stream to send the request
	    outstream = con.getOutputStream();

	    ObjectOutputStream oos = null;

	    try {
		oos = new ObjectOutputStream(outstream);
		// We serialize the object which contend the request parameter
		oos.writeObject(webRequest);
	    } finally {
		if (oos != null) {
		    oos.flush();
		    oos.close();
		    outstream.close();
		}
	    }
	    // We get the response.
	    // System.out.println(i + " sendWebRequest getting answer ");
	    InputStream instr = null;
	    ObjectInputStream inputFromServlet = null;
	    try {
		instr = con.getInputStream();

		// we get the session id
		final String strCookieValue = con.getHeaderField("Set-cookie");
		final String sessionID = con.getHeaderField("TANGO_SESSION_ID");
		// we get the jsessionid
		if (strCookieValue != null) {
		    // We get the the JSessionID to use that in the next
		    // request.
		    // the next request use the same httpsession
		    final int iBegin = strCookieValue.indexOf("JSESSIONID=");
		    if (iBegin != -1) {
			final int iEnd = strCookieValue.indexOf(";");
			if (iEnd != -1) {
			    m_strJsessionid = strCookieValue.substring(iBegin, iEnd);
			} else {
			    m_strJsessionid = strCookieValue.substring(iBegin);
			}

		    }
		    if (m_strJsessionid == null) {
			m_strJsessionid = sessionID.trim();
		    }
		} else {
		    if (m_strJsessionid == null && sessionID != null) {
			m_strJsessionid = sessionID.trim();
		    }
		}

		// System.out.println("m_strJsessionid :" + m_strJsessionid);
		// System.out.println("sessionID :'" + sessionID+"'");
		inputFromServlet = new ObjectInputStream(instr);
		// We get the serialized object
		response = (WebResponse) inputFromServlet.readObject();
		// System.out.println(i + "response " + response);
		// if (response != null) {
		// System.out.println(i + "results " +
		// response.getResult().getClass().getCanonicalName());
		// }
	    } finally {
		if (inputFromServlet != null) {
		    inputFromServlet.close();
		}
		if (instr != null) {
		    instr.close();
		}
	    }

	    UtilLogger.logger.addInfoLog("##########WebServerClient.getObject end :" + m_Url);
	    // We test if the response contains an exception
	    if (response == null) {
		return null;
	    }
	    if (response.getResult() == null) {
		return null;
	    }

	    // We check if the first element is an Exception or not.
	    // If true, a problem occur in the web server
	    if (response.getResult().length > 0) {
		final Object objet = response.getResult()[0];
		if (objet instanceof Exception) {
		    // System.err.println(i + "XXXXXXX ERROR send by server " +
		    // objet);
		    final Exception e = (Exception) objet;
		    final StringWriter sw = new StringWriter();
		    if (e.getCause() != null) {
			e.getCause().printStackTrace(new PrintWriter(sw));
		    } else {
			e.printStackTrace(new PrintWriter(sw));
		    }
		    final String stacktrace = sw.toString();
		    System.err.println(i + "XXXXXXX ERROR send by server: " + stacktrace);
		    throw (Exception) objet;
		}
	    }
	    // System.out.println(i + "##############sendWebRequest end OK ");
	} catch (final SoleilException e) {
	    System.err.println(i + " request failed " + e);
	    System.out.println("m_strJsessionid :" + m_strJsessionid);
	    throw e;
	} catch (final WebSecurityException wse) {
	    System.err.println(i + " request failed " + wse);
	    System.out.println("m_strJsessionid :" + m_strJsessionid);
	    if (m_iLoginAction != null && WebSecurityException.USER_NOT_CONNECTED.equals(wse.getExceptionCode())) {

		// we call the loginview
		m_iLoginAction.authenticateUser();
		// we relaunch the request
		return sendWebRequest(webRequest);
	    } else {
		throw wse;
	    }
	} catch (final MalformedURLException ex) {
	    System.err.println(i + " request failed " + ex);
	    ex.printStackTrace();
	    UtilLogger.logger.addFATALLog(ex);
	    throw new WebServerClientException(WebServerClientException.s_str_URL_MALFORMED,
		    WebServerClientException.FATAL, ex.getMessage());
	} catch (final FileNotFoundException ex) {
	    System.err.println(i + " request failed " + ex);
	    ex.printStackTrace();
	    UtilLogger.logger.addFATALLog("Failed to open stream to URL: " + ex);
	    throw new WebServerClientException(WebServerClientException.s_str_URL_NOT_FOUND,
		    WebServerClientException.FATAL, ex.getMessage());
	} catch (final IOException ex) {
	    System.err.println(i + " request failed " + ex);
	    ex.printStackTrace();
	    UtilLogger.logger.addFATALLog("Error reading URL content: " + ex);
	    throw new WebServerClientException(WebServerClientException.s_str_ERROR_READING_URL_CONTENT,
		    WebServerClientException.FATAL, ex.getMessage());
	} catch (final ClassNotFoundException cnfe) {
	    System.err.println(i + " request failed " + cnfe);
	    cnfe.printStackTrace();
	    UtilLogger.logger.addFATALLog("CLasse not found excpetion" + cnfe);
	    throw new WebServerClientException(WebServerClientException.s_str_CLASS_NOT_FOUND,
		    WebServerClientException.FATAL, cnfe.getMessage());
	}

	return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.util.serialized.IWebServerClient#getObject(fr.soleil.util.
     * serialized.WebRequest)
     */
    @Override
    public synchronized WebResponse getObject(final WebRequest webRequest) throws Exception {
	// TEMP find a better solution ------------------
	if (m_recallManager != null && !m_recallManager.getStatus()) {
	    throw new Exception();

	}

	return getResponse(webRequest);

    }

    /**
     * Get a web response for a web request
     * 
     * @param webRequest
     *            {@link WebRequest}
     * @return {@link WebResponse}
     * @throws Exception
     *             : Throws a webServerClientException if we can't establish a
     *             connection with the server. Throws a soleil exception if we
     *             have a problem. Throws other exception otherwise
     * 
     */
    private WebResponse getResponse(final WebRequest webRequest) throws Exception {
	try {
	    // we put the code application in the webRequest to identifie the
	    // sender
	    if (webRequest != null) {
		webRequest.setApplication(getApplication());
	    }

	    final WebResponse response = sendWebRequest(webRequest);
	    if (m_recallManager != null) {
		m_recallManager.setStatus(true);
	    }

	    return response;
	} catch (final WebServerClientException e) {
	    // we call the racel manager to get if we need to do an another call
	    // to the server
	    if (m_recallManager != null && m_recallManager.call(e.getTechComment())) {
		m_recallManager.setStatus(false);
		m_recallManager.setMessage(e.getTechComment());

		return getResponse(webRequest);
	    } else {
		throw e;
	    }
	} catch (final SoleilException e) {
	    throw e;
	} catch (final Exception e) {
	    System.err.println("WebServerClient.getResponse(WebRequest webRequest) :" + e.getMessage());

	    throw e;
	}
    }

    /**
     * Return an URLConnection to send a query to the server.
     * 
     * @param strServlet_name
     * @return URLConnection
     * @throws MalformedURLException
     * @throws IOException
     */
    private URLConnection getServletConnection(final String strServlet_name) throws MalformedURLException, IOException {

	URL urlServlet = null;
	if (strServlet_name == null) {
	    urlServlet = m_Url;
	} else {
	    urlServlet = new URL(m_Url, strServlet_name);
	}
	final URLConnection connection = urlServlet.openConnection();
	// we add a timeout
	connection.setConnectTimeout(180000);
	connection.setDoInput(true);
	connection.setDoOutput(true);
	// don't use cache ... it's better to have the correct response
	connection.setUseCaches(false);

	// We must specified set content type to URLConnection
	connection.setRequestProperty("Content-Type", "application/x-java-serialized-object");

	// we add the jsessionid in the request to specify the httpsession used.
	if (m_strJsessionid != null) {
	    // we add the server sessionid
	    connection.setRequestProperty("Cookie", m_strJsessionid);
	}

	return connection;

    }

    // The getter and setter of the url

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.util.serialized.IWebServerClient#getUrl()
     */
    public URL getUrl() {
	return m_Url;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.util.serialized.IWebServerClient#setUrl(java.net.URL)
     */
    public void setUrl(final URL url) {
	m_Url = url;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.util.serialized.IWebServerClient#getRecallManager()
     */
    @Override
    public IRecallManager getRecallManager() {
	return m_recallManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.util.serialized.IWebServerClient#setRecallManager(fr.soleil
     * .util.serialized.IRecallManager)
     */
    @Override
    public void setRecallManager(final IRecallManager manager) {
	m_recallManager = manager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.util.serialized.IWebServerClient#getM_iLoginAction()
     */
    @Override
    public ILoginAction getM_iLoginAction() {
	return m_iLoginAction;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.util.serialized.IWebServerClient#setM_iLoginAction(fr.soleil
     * .util.serialized.ILoginAction)
     */
    @Override
    public void setM_iLoginAction(final ILoginAction loginAction) {
	m_iLoginAction = loginAction;
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.util.serialized.IWebServerClient#getApplication()
     */
    @Override
    public String getApplication() {
	return m_strApplication;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * fr.soleil.util.serialized.IWebServerClient#setApplication(java.lang.String
     * )
     */
    @Override
    public void setApplication(final String application) {
	m_strApplication = application;
    }

    public String getM_strJsessionid() {
	return m_strJsessionid;
    }

    public void setM_strJsessionid(final String jsessionid) {
	m_strJsessionid = jsessionid;
    }
}
