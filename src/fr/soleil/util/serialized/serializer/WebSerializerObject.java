package fr.soleil.util.serialized.serializer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains Value for a non-serialized object
 * 
 * @author BARBA-ROSSA
 *
 */
public class WebSerializerObject implements Serializable {

    private static final long serialVersionUID = 2490450619372796202L;

    private String m_ObjectID = null;
    private String m_strClass = null;
    private Map<String, Object> m_attributes = null;

    public WebSerializerObject(String strClass) {
        m_strClass = strClass;
        m_attributes = new HashMap<String, Object>();
    }

    /**
     * Add an attribut in the serialized class
     * 
     * @param attrName
     * @param attrValue
     */
    public void addAttribute(String attrName, Object attrValue) {
        m_attributes.put(attrName, attrValue);
    }

    /**
     * return true if the attrName is an Attribute.
     * 
     * @param attrName
     * @return boolean
     */
    public boolean isAttribute(String attrName) {
        return m_attributes.containsKey(attrName);
    }

    public Object getAttribute(String attrName) {
        return m_attributes.get(attrName);
    }

    public void removeAttribute(String attrName) {
        m_attributes.remove(attrName);
    }

    public String getClazz() {
        return m_strClass;
    }

    public void setClazz(String class1) {
        m_strClass = class1;
    }

    public String getObjectID() {
        return m_ObjectID;
    }

    public void setObjectID(String objectID) {
        m_ObjectID = objectID;
    }

}
