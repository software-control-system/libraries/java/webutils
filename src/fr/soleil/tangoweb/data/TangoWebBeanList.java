package fr.soleil.tangoweb.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TangoWebBeanList implements Serializable {

    private static final long serialVersionUID = -9020003281565317277L;

    private Map<String, TangoWebBean> m_beanMap = null;

    public TangoWebBeanList() {
        m_beanMap = new HashMap<String, TangoWebBean>();
    }

    public Map<String, TangoWebBean> getMap() {
        return m_beanMap;
    }

    public void setMap(Map<String, TangoWebBean> map) {
        m_beanMap = map;
    }

    public void addBean(TangoWebBean newBean) {
        if (newBean != null) {
            m_beanMap.put(newBean.getClassName(), newBean);
        }
    }

    public TangoWebBean getBean(String className) {
        TangoWebBean bean;
        if (className == null) {
            bean = null;
        } else {
            bean = m_beanMap.get(className);
        }
        return bean;
    }
}
